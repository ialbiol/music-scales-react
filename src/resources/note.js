export class Note {
  id: number;
  note: string;
  name: string;
  octave: string;
  index: string;
}