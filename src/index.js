import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import Scales from './Scales';
import Pentagrama from './Pentagrama';
import Guitar from './Guitar';

import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Scales />, document.getElementById('scalesApp'));
ReactDOM.render(<Pentagrama />, document.getElementById('pentagramaApp'));
ReactDOM.render(<Guitar />, document.getElementById('guitarApp'));
registerServiceWorker();
