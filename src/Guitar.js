import React, { Component } from 'react';

import { ScalesService } from './resources/scales-service';
//import { Note } from './resources/note';

import './Guitar.css';

class Guitar extends Component {

  constructor(props) {
    super(props);

    this.state = {
      selnote: 3,
      scale: 'Colour',
      scalePosition: 1,
      fredsMove: 0
    };

    this.GUITAR_FREDS = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24];
    this.GUITAR_CORDS = [1, 2, 3, 4, 5, 6];

    this.handleGuitar = this.handleGuitar.bind(this);
  }

  handleGuitar(event) {
    //console.log("note: " + event.target.dataset.note);
    //console.log("scale: " + event.target.dataset.scale);
    //console.log("position: " + event.target.dataset.position);
    //console.log("fredsmove: " + event.target.dataset.fredsmove);

    if(event.target.dataset.note){
      var selNote = +event.target.dataset.note;
      this.setState({selnote: selNote});
    }

    if(event.target.dataset.scale){
      var selScale = event.target.dataset.scale;
      this.setState({scale: selScale});
    }

    if(event.target.dataset.position){
      var selPosition = +event.target.dataset.position;
      this.setState({scalePosition: selPosition});
    }

    if(event.target.dataset.fredsmove){
      var selFredsmove = +event.target.dataset.fredsmove;
      this.setState({fredsMove: selFredsmove});
    }

    event.preventDefault();
  }

  render() {
    //calculate the scale
    var ScalesSrv = new ScalesService();
    var newGuitar = "";

    if(this.state.scale === "Colour"){
      newGuitar = ScalesSrv.getGuitar();
    }
    if(this.state.scale === "Major"){
      newGuitar = ScalesSrv.getGuitarMajorScale(this.state.scalePosition, this.state.fredsMove);
    }
    if(this.state.scale === "Pentatonic"){
      newGuitar = ScalesSrv.getGuitarPentatonicScale(this.state.scalePosition, true, 1);
    }

    return (
      <div className="Guitar">
        Colour scale:<br />
        <table>
          <tbody>
            <tr>
              <td><input type="button" value="A" data-note="0" data-scale="Colour" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="B" data-note="2" data-scale="Colour" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="C" data-note="3" data-scale="Colour" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="D" data-note="5" data-scale="Colour" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="E" data-note="7" data-scale="Colour" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="F" data-note="8" data-scale="Colour" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="G" data-note="10" data-scale="Colour" onClick={this.handleGuitar} /></td>
            </tr>
          </tbody>
        </table>

        Major scale:<br />
        <table>
          <tbody>
            <tr>
              <td><input type="button" value="A" data-note="0" data-scale="Major" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="B" data-note="2" data-scale="Major" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="C" data-note="3" data-scale="Major" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="D" data-note="5" data-scale="Major" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="E" data-note="7" data-scale="Major" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="F" data-note="8" data-scale="Major" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="G" data-note="10" data-scale="Major" onClick={this.handleGuitar} /></td>
            </tr>
          </tbody>
        </table>

        <table>
          <tbody>
            <tr>
              <td><input type="button" value="1" data-note="" data-scale="Major" data-position="1" data-fredsmove="" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="2" data-note="" data-scale="Major" data-position="2" data-fredsmove="" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="3" data-note="" data-scale="Major" data-position="3" data-fredsmove="" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="4" data-note="" data-scale="Major" data-position="4" data-fredsmove="" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="5" data-note="" data-scale="Major" data-position="5" data-fredsmove="" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="6" data-note="" data-scale="Major" data-position="6" data-fredsmove="" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="7" data-note="" data-scale="Major" data-position="7" data-fredsmove="" onClick={this.handleGuitar} /></td>
            </tr>
          </tbody>
        </table>

        Pentatonic scale:<br />
        <table>
          <tbody>
            <tr>
              <td><input type="button" value="A" data-note="0" data-scale="Pentatonic" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="B" data-note="2" data-scale="Pentatonic" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="C" data-note="3" data-scale="Pentatonic" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="D" data-note="5" data-scale="Pentatonic" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="E" data-note="7" data-scale="Pentatonic" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="F" data-note="8" data-scale="Pentatonic" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="G" data-note="10" data-scale="Pentatonic" onClick={this.handleGuitar} /></td>
            </tr>
          </tbody>
        </table>

        <table>
          <tbody>
            <tr>
              <td><input type="button" value="1" data-note="" data-scale="Pentatonic" data-position="1" data-fredsmove="" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="2" data-note="" data-scale="Pentatonic" data-position="2" data-fredsmove="" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="3" data-note="" data-scale="Pentatonic" data-position="3" data-fredsmove="" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="4" data-note="" data-scale="Pentatonic" data-position="4" data-fredsmove="" onClick={this.handleGuitar} /></td>
              <td><input type="button" value="5" data-note="" data-scale="Pentatonic" data-position="5" data-fredsmove="" onClick={this.handleGuitar} /></td>
            </tr>
          </tbody>
        </table>

        <br />

        Guitar<br />

        <div className='mainGuitar'>
        <div className='headerGuitar'>

        {this.GUITAR_FREDS.map((fred) =>
          <div key={fred} className={"GuitarNote Fred-" + fred}>{fred}</div>
        )}

        </div>
        
        <div className='bodyGuitar'>

        {this.GUITAR_CORDS.map((cord) =>
          <div key={"c-" + cord} className={"GuitarCord Cord-" + cord}></div>
        )}

        {this.GUITAR_FREDS.map((fred) =>
          <div key={"f-" + fred} className={"GuitarFred Fred-" + fred}></div>
        )}
  
        {
            newGuitar.map(function(guitar, cord) {
                var guitarNotes = guitar.map(function(note, fred) {
                    return (
                      <div key={note.index} className={"GuitarNote Cord-" + (cord + 1) + " Fred-" + fred}>{note.note}</div>
                    )
                });

                return (
                  guitarNotes
                )
            })
        }

        </div>
        </div>
      </div>
    );
  }
}

export default Guitar;
