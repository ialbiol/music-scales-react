import React, { Component } from 'react';

import { TRANSLATION } from './resources/mock-notes';

import './Pentagrama.css';

function ScalesNotesG(props) {
  if(props.props.option === 'Scales'){
  return (
  <div className='mainPentagrama'>
   <div className='pentaLine1'></div>
   <div className='pentaLine2'></div>
   <div className='pentaLine3'></div>
   <div className='pentaLine4'></div>
   <div className='pentaLine5'></div>

    <div className='key key-G-1-C position-15'>C</div>
    <div className='key key-G-1-D position-16'>D</div>
    <div className='key key-G-1-E position-17'>E</div>
    <div className='key key-G-1-F position-18'>F</div>
    <div className='key key-G-1-G position-19'>G</div>
    <div className='key key-G-1-A position-20'>A</div>
    <div className='key key-G-1-B position-21'>B</div>
  
    <div className='key key-G-2-C position-22'>C</div>
    <div className='key key-G-2-D position-23'>D</div>
    <div className='key key-G-2-E position-24'>E</div>
    <div className='key key-G-2-F position-25'>F</div>
    <div className='key key-G-2-G position-26'>G</div>
    <div className='key key-G-2-A position-27'>A</div>
    <div className='key key-G-2-B position-28'>B</div>
   </div>
  );
 } else {
  return '';
 }
}

function ScalesNotesA(props) {
  if(props.props.option === 'Scales'){
   return (
    <div className='mainPentagrama'>
    <div className='pentaLine1'></div>
    <div className='pentaLine2'></div>
    <div className='pentaLine3'></div>
    <div className='pentaLine4'></div>
    <div className='pentaLine5'></div>
    
    <div className='key key-F-1-D position-1'>D</div>
    <div className='key key-F-1-E position-2'>E</div>
    <div className='key key-F-1-F position-3'>F</div>
    <div className='key key-F-1-G position-4'>G</div>
    <div className='key key-F-1-A position-5'>A</div>
    <div className='key key-F-1-B position-6'>B</div>
    <div className='key key-F-1-C position-7'>C</div>
    
    <div className='key key-F-2-D position-8'>D</div>
    <div className='key key-F-2-E position-9'>E</div>
    <div className='key key-F-2-F position-10'>F</div>
    <div className='key key-F-2-G position-11'>G</div>
    <div className='key key-F-2-A position-12'>A</div>
    <div className='key key-F-2-B position-13'>B</div>
    <div className='key key-F-2-C position-14'>C</div>
    </div>
  );
 } else {
  return '';
 }
}

function PlayNotesG(props) {
  if(props.props.option !== 'Scales'){
    if(props.props.PlayPentagrama === 'G'){
      return (
        <div className='mainPentagrama'>
         <div className='pentaLine1'></div>
         <div className='pentaLine2'></div>
         <div className='pentaLine3'></div>
         <div className='pentaLine4'></div>
         <div className='pentaLine5'></div>
      
          <div className={"key key-G-" + props.props.PlayScale + '-' + props.props.PlayNote.note + ' position-1'}></div>
      
        </div>
        );
    } else {
      return (
        <div className='mainPentagrama'>
         <div className='pentaLine1'></div>
         <div className='pentaLine2'></div>
         <div className='pentaLine3'></div>
         <div className='pentaLine4'></div>
         <div className='pentaLine5'></div>
        </div>
        );
    }
 } else {
  return '';
 }
}


function PlayNotesA(props) {
  if(props.props.option !== 'Scales'){
    if(props.props.PlayPentagrama === 'A'){
      return (
        <div className='mainPentagrama'>
         <div className='pentaLine1'></div>
         <div className='pentaLine2'></div>
         <div className='pentaLine3'></div>
         <div className='pentaLine4'></div>
         <div className='pentaLine5'></div>
      
          <div className={"key key-F-" + props.props.PlayScale + '-' + props.props.PlayNote.note + ' position-1'}></div>
      
        </div>
        );
    } else {
      return (
        <div className='mainPentagrama'>
         <div className='pentaLine1'></div>
         <div className='pentaLine2'></div>
         <div className='pentaLine3'></div>
         <div className='pentaLine4'></div>
         <div className='pentaLine5'></div>
        </div>
        );
    }
 } else {
  return '';
 }
}


class Pentagrama extends Component {

  constructor(props) {
    super(props);

    this.state = {
      option: 'Scales',
      PlayOk: 0,
      PlayKo: 0,
      PlayPentagrama: 'G',
      PlayScale: 1,
      PlayNote: 'A',
    };

    this.PENTA_NOTES = [0, 2, 3, 5, 7, 8, 10];

    this.handlePentagramaOptions = this.handlePentagramaOptions.bind(this);
    this.handlePentagramaPlay = this.handlePentagramaPlay.bind(this);
  }

  handlePentagramaOptions(event) {
    //console.log("option: " + event.target.dataset.option);

    var selOption = event.target.dataset.option;
    this.setState({option: selOption});

    this.doPlayingNote();

    event.preventDefault();
  }

  handlePentagramaPlay(event) {
    if(this.state.option !== 'Scales'){
      var selNote = event.target.value;

      if(selNote.toUpperCase() === this.state.PlayNote.note){
        var num = this.state.PlayOk + 1;
        this.setState({PlayOk: num});
      } else {
        var num = this.state.PlayKo + 1;
        this.setState({PlayKo: num});
      }

      this.doPlayingNote();
    }

    event.preventDefault();
  }

  doPlayingNote(){
    if(this.state.option === "PlayG"){
      this.setState({PlayPentagrama: 'G'});
    }
    if(this.state.option === "PlayA"){
      this.setState({PlayPentagrama: 'A'});
    }
    if(this.state.option === "PlayBoth"){
      var randPentagrama = Math.floor(Math.random() * 2) + 1;
      if(randPentagrama === 1){
        this.setState({PlayPentagrama: 'G'});
      } else {
        this.setState({PlayPentagrama: 'A'});
      }
    }

    var randScale = Math.floor(Math.random() * 2) + 1;
    var randNoteNum = this.PENTA_NOTES[Math.floor(Math.random() * 7)];
    var randNote = TRANSLATION[randNoteNum];
    this.setState({PlayScale: randScale});
    this.setState({PlayNote: randNote});
  }

  render() {
    return (
      <div className="Pentagrama">
        Options:<br />
        <table>
          <tbody>
            <tr>
              <td><input type="button" value="Scales" data-option="Scales" onClick={this.handlePentagramaOptions} /></td>
              <td>Play: </td>
              <td><input type="button" value="G" data-option="PlayG" onClick={this.handlePentagramaOptions} /></td>
              <td><input type="button" value="A" data-option="PlayA" onClick={this.handlePentagramaOptions} /></td>
              <td><input type="button" value="Both" data-option="PlayBoth" onClick={this.handlePentagramaOptions} /></td>
              <td>OK: {this.state.PlayOk}</td>
              <td>Error: {this.state.PlayKo}</td>
              <td><input type="text" value="" onChange={this.handlePentagramaPlay} /></td>

            </tr>
          </tbody>
        </table>

        pentagrama:<br />

        <ScalesNotesG props={this.state}/>
        <ScalesNotesA props={this.state}/>
        <PlayNotesG props={this.state}/>
        <PlayNotesA props={this.state}/>


      </div>
    );
  }
}

export default Pentagrama;
