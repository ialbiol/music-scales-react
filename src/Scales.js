import React, { Component } from 'react';

import { ScalesService } from './resources/scales-service';

function ListNote(props) {
  // Correct! There is no need to specify the key here:
  return <td>{props.value.note}</td>;
}

function DrawScale(props) {
  const scale = props.scale;

  const listNotes = scale.map((note) =>
    // Correct! Key should be specified inside the array.
    <ListNote key={note.index}
              value={note} />

  );

  return (
    <table>
      <tbody>
      <tr>
        {listNotes}
      </tr>
      </tbody>
    </table>
  );
}

class Scales extends Component {

  constructor(props) {
    super(props);

    this.state = {
      inputScale: 3,
      scale: 'Colour'
    };

    this.handleScale = this.handleScale.bind(this);
  }

  handleScale(event) {
    var selNote = +event.target.dataset.note;
    var selScale = event.target.dataset.scale;
    this.setState({inputScale: selNote});
    this.setState({scale: selScale});

    event.preventDefault();
  }

  render() {
    //calculate the scale
    var ScalesSrv = new ScalesService();
    var newScale = "";

    if(this.state.scale === "Colour"){
      newScale = ScalesSrv.getColourScale(this.state.inputScale, "1");
    }
    if(this.state.scale === "Major"){
      newScale = ScalesSrv.getMajorScale(this.state.inputScale);
    }

    return (
      <div className="Scales">
        Colour scale:<br />
        <table>
          <tbody>
            <tr>
              <td><input type="button" value="A" data-note="0" data-scale="Colour" onClick={this.handleScale} /></td>
              <td><input type="button" value="B" data-note="2" data-scale="Colour" onClick={this.handleScale} /></td>
              <td><input type="button" value="C" data-note="3" data-scale="Colour" onClick={this.handleScale} /></td>
              <td><input type="button" value="D" data-note="5" data-scale="Colour" onClick={this.handleScale} /></td>
              <td><input type="button" value="E" data-note="7" data-scale="Colour" onClick={this.handleScale} /></td>
              <td><input type="button" value="F" data-note="8" data-scale="Colour" onClick={this.handleScale} /></td>
              <td><input type="button" value="G" data-note="10" data-scale="Colour" onClick={this.handleScale} /></td>
            </tr>
          </tbody>
        </table>

        Major scale:<br />
        <table>
          <tbody>
            <tr>
              <td><input type="button" value="A" data-note="0" data-scale="Major" onClick={this.handleScale} /></td>
              <td><input type="button" value="B" data-note="2" data-scale="Major" onClick={this.handleScale} /></td>
              <td><input type="button" value="C" data-note="3" data-scale="Major" onClick={this.handleScale} /></td>
              <td><input type="button" value="D" data-note="5" data-scale="Major" onClick={this.handleScale} /></td>
              <td><input type="button" value="E" data-note="7" data-scale="Major" onClick={this.handleScale} /></td>
              <td><input type="button" value="F" data-note="8" data-scale="Major" onClick={this.handleScale} /></td>
              <td><input type="button" value="G" data-note="10" data-scale="Major" onClick={this.handleScale} /></td>
            </tr>
          </tbody>
        </table>

        <br />
        Scale:<br />
        <DrawScale scale={newScale} />
      </div>
    );
  }
}

export default Scales;
